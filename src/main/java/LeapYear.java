public class LeapYear {

    public static boolean isLeapYear(int inYear) {

        if(inYear % 4 == 0  && (inYear % 100 != 0 || inYear % 400 == 0)){
           return true;
        }
        return false;
    }
}
