import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class LeapYearTest {
    /**
     *
     * Write a function that returns true or false depending on whether its input integer is a leap year or not.
     *
     * A leap year is defined as one that is divisible by 4, but is not otherwise divisible by 100 unless it is also divisible by 400.
     *
     * For example, 2001 is a typical common year and 1996 is a typical leap year, whereas 1900 is an atypical common year and 2000 is an atypical leap year.
     *
     *
     * 2001 is a regular year
     * 1996 is a leap year
     * 1900 atypical regular year
     * 2000 atypical leap year
     */

    @Test
    public void isNotLeapYear(){

       boolean isLeapYear =  LeapYear.isLeapYear(2001);
       assertFalse(isLeapYear);
    }

    @Test
    public void isLeapYear(){

        boolean isLeapYear =  LeapYear.isLeapYear(1996);
        assertTrue(isLeapYear);
    }

    @Test
    public void isAtypicalNonLeapYear(){
        boolean isLeapYear =  LeapYear.isLeapYear(1900);
        assertFalse(isLeapYear);

    }

    @Test
    public void isAtypicalLeapYear(){
        boolean isLeapYear =  LeapYear.isLeapYear(2000);
        assertTrue(isLeapYear);

    }

}
